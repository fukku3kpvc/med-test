source 'https://rubygems.org'

git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.1'

# -- Core -- #
gem 'bootsnap', '>= 1.1.0', require: false # Boot large Ruby/Rails apps faster. (https://github.com/Shopify/bootsnap)
gem 'config' # Effortless multi-environment settings in Rails, Sinatra, Pandrino and others (https://github.com/railsconfig/config)
gem 'puma', '~> 3.11' # A Ruby/Rack web server built for concurrency. (https://github.com/puma/puma)
gem 'rails', '~> 5.2.4' # Full-stack web application framework. (http://rubyonrails.org)
gem 'time_difference' # TimeDifference is the missing Ruby method to calculate difference between two given time. You can do a Ruby time difference in year, month, week, day, hour, minute, and seconds.
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby] # Timezone Data for TZInfo. (https://github.com/tzinfo/tzinfo-data)

# -- Infrastructure & Storages -- #
gem 'aasm' # State machine mixin for Ruby objects (https://github.com/aasm/aasm)
gem 'devise' # Flexible authentication solution for Rails with Warden. (https://github.com/plataformatec/devise)
gem 'pg', '>= 0.18', '< 2.0' # Pg is the Ruby interface to the {PostgreSQL RDBMS}[http://www.postgresql.org/] (https://bitbucket.org/ged/ruby-pg)
gem 'rails-settings-cached', '~> 2.0' # Global settings for your Rails application. (https://github.com/huacnlee/rails-settings-cached)
gem 'ffaker' # Ffaker generates dummy data. (https://github.com/ffaker/ffaker)

# -- Frontend -- #
gem 'coffee-rails', '~> 4.2' # CoffeeScript adapter for the Rails asset pipeline. Also adds support for .coffee views. (https://github.com/rails/coffee-rails)
gem 'haml-rails', '~> 2.0' # Haml-rails provides Haml generators for Rails 5. (https://github.com/haml/haml-rails)
gem 'jbuilder', '~> 2.5' # Build JSON APIs with ease. (https://github.com/rails/jbuilder)
gem 'jquery-rails' # Use jQuery with Rails 4+ (https://github.com/rails/jquery-rails)
gem 'sass-rails', '~> 5.0' # Ruby on Rails stylesheet engine for Sass. (https://github.com/rails/sass-rails)
gem 'simple_form' # Forms made easy! (https://github.com/plataformatec/simple_form)
gem 'turbolinks', '~> 5' # Turbolinks makes navigating your web application faster. (https://github.com/turbolinks/turbolinks)
gem 'uglifier', '>= 1.3.0' # Ruby wrapper for UglifyJS JavaScript compressor. (https://github.com/lautis/uglifier)

group :development, :test do
  # -- Testing structure -- #
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw] # Ruby fast debugger - base + CLI (https://github.com/deivid-rodriguez/byebug)
  gem 'factory_bot_rails' # factory_bot_rails provides integration between factory_bot and rails 4.2 or newer (https://github.com/thoughtbot/factory_bot_rails)
  gem 'pry-byebug', platform: :mri # Fast debugging with Pry. (https://github.com/deivid-rodriguez/pry-byebug)
  gem 'pry-rails' # Use Pry as your rails console (https://github.com/rweng/pry-rails)
  gem 'rails-controller-testing' # Extracting `assigns` and `assert_template` from ActionDispatch. (https://github.com/rails/rails-controller-testing)
  gem 'rspec' # rspec-3.8.0 (http://github.com/rspec)
  gem 'rspec-rails' # RSpec for Rails (https://github.com/rspec/rspec-rails)
  gem 'shoulda-matchers', '~> 4.0' # Making tests easy on the fingers and eyes (https://matchers.shoulda.io/)

  # -- Annotations -- #
  gem 'annotate' # Annotates Rails Models, routes, fixtures, and others based on the database schema. (http://github.com/ctran/annotate_models)
  gem 'annotate_gem' # Add comments to your Gemfile with each dependency's description. (https://github.com/ivantsepp/annotate_gem)
end

group :development do
  gem 'bullet' # help to kill N+1 queries and unused eager loading (https://github.com/flyerhzm/bullet)
  gem 'listen', '>= 3.0.5', '< 3.2' # Listen to file modifications (https://github.com/guard/listen)
  gem 'spring' # Rails application preloader (https://github.com/rails/spring)
  gem 'spring-watcher-listen', '~> 2.0.0' # Makes spring watch files using the listen gem. (https://github.com/jonleighton/spring-watcher-listen)
  gem 'web-console', '>= 3.3.0' # A debugging tool for your Ruby on Rails applications. (https://github.com/rails/web-console)
end

group :test do
  # -- Test supports -- #
  gem 'simplecov' # Code coverage for Ruby 1.9+ with a powerful configuration library and automatic merging of coverage across test suites (http://github.com/colszowka/simplecov)
  gem 'test-prof' # Ruby applications tests profiling tools (http://github.com/palkan/test-prof)
  gem 'timecop' # A gem providing "time travel" and "time freezing" capabilities, making it dead simple to test time-dependent code.  It provides a unified method to mock Time.now, Date.today, and DateTime.now in a single call. (https://github.com/travisjeffery/timecop)
  gem 'webmock' # Library for stubbing HTTP requests in Ruby. (http://github.com/bblimke/webmock)
end
