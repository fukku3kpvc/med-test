Rails.application.routes.draw do
  devise_for :users

  scope :admin do
    get '/', to: 'admins#index'
    resources :users, except: %I[index new]
  end

  resources :tests, only: %I[create show update]

  root to: 'users#show'
end
