FactoryBot.define do
  factory :test do
    
  end
end

# == Schema Information
#
# Table name: tests
#
#  id                         :bigint           not null, primary key
#  correct_answers            :integer          default(0), not null
#  correct_answers_percentage :integer          default(0), not null
#  is_solved                  :boolean          default(FALSE), not null
#  questions_count            :integer          default(0), not null
#  solved_at                  :datetime
#  solving_time               :integer
#  state                      :enum             default("started"), not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  user_id                    :bigint           not null
#
# Indexes
#
#  index_tests_on_user_id  (user_id)
#
