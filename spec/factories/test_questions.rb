FactoryBot.define do
  factory :test_question do
    
  end
end

# == Schema Information
#
# Table name: test_questions
#
#  id          :bigint           not null, primary key
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  answer_id   :bigint
#  question_id :bigint           not null
#  test_id     :bigint           not null
#
# Indexes
#
#  index_test_questions_on_answer_id    (answer_id)
#  index_test_questions_on_question_id  (question_id)
#  index_test_questions_on_test_id      (test_id)
#
