FactoryBot.define do
  factory :user do
    
  end
end

# == Schema Information
#
# Table name: users
#
#  id                  :bigint           not null, primary key
#  encrypted_password  :string           default(""), not null
#  full_name           :string           default(""), not null
#  login               :string           default(""), not null
#  remember_created_at :datetime
#  stored_password     :string           default(""), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_users_on_login  (login) UNIQUE
#
