FactoryBot.define do
  factory :question do
    
  end
end

# == Schema Information
#
# Table name: questions
#
#  id                :bigint           not null, primary key
#  context           :text             default(""), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  question_block_id :bigint           not null
#
# Indexes
#
#  index_questions_on_question_block_id  (question_block_id)
#
