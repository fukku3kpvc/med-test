class UsersController < ApplicationController
  http_basic_authenticate_with Settings.ba.to_h.merge(except: :show)
  before_action :authenticate_user!, only: :show
  before_action :find_user, only: %I[edit update destroy]

  # GET /
  def show
    @tests = current_user.tests.order(created_at: :desc)
  end

  # POST /admin/users
  def create
    @user = User.new_self(user_params.to_h['full_name'])
    if @user.save
      redirect_to '/admin', notice: Settings.notices.user_created
    else
      redirect_to '/admin', alert: user_errors
    end
  end

  # GET /admin/users/:id
  def edit; end

  # PUT /admin/users/:id
  def update
    if @user.update(user_params)
      redirect_to '/admin', notice: Settings.notices.user_updated
    else
      redirect_to '/admin', alert: user_errors
    end
  end

  # DELETE /admin/users/:id
  def destroy
    @user.destroy
    if @user.destroyed?
      flash[:notice] = Settings.notices.user_destroyed
    else
      flash[:alert] = user_errors
    end
    redirect_to '/admin'
  end

  private

  def user_errors
    @user.errors.full_messages.to_sentence
  end

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:full_name)
  end
end
