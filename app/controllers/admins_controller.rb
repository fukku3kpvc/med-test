class AdminsController < ApplicationController
  http_basic_authenticate_with Settings.ba.to_h

  # GET /admin
  def index
    @users = User.includes(:tests).all.order(created_at: :desc)
    @crypt = Encryptor.new
    @user = User.new
  end
end
