class TestsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_test, only: %I[show update]
  before_action :check_for_open_tests, only: :create

  # POST /tests
  def create
    @test = Test.generate(current_user)
    if @test.save
      redirect_to test_path(@test), notice: Settings.notices.test_started
    else
      redirect_to root_path, alert: test_errors
    end
  end

  # GET /tests/:id
  def show; end

  # PUT /tests/:id
  def update
    @test.resolve(params.to_unsafe_hash[:test_question])
    if @test.updated_at.to_i != @test.created_at.to_i
      @test.finish!
      redirect_to test_path(@test), notice: Settings.notices.test_finished
    else
      redirect_to root_path, alert: Settings.alerts.test_failed
    end
  end

  private

  def test_errors
    @test.errors.full_messages.to_sentence
  end

  def find_test
    @test = current_user.tests
                        .includes(
                          test_questions: [{ question: :answers }, :answer]
                        ).find(params[:id])
  end

  def check_for_open_tests
    if current_user.tests.started.any?
      redirect_to test_path(current_user.tests.last),
                  alert: Settings.alerts.existing_test
    end
  end
end