class Question < ApplicationRecord
  belongs_to :question_block
  has_many :answers, dependent: :destroy
  has_many :test_questions, dependent: :destroy
  has_many :tests, through: :test_questions

  validates :context, presence: true
end

# == Schema Information
#
# Table name: questions
#
#  id                :bigint           not null, primary key
#  context           :text             default(""), not null
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  question_block_id :bigint           not null
#
# Indexes
#
#  index_questions_on_question_block_id  (question_block_id)
#
