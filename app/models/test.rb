class Test < ApplicationRecord
  include AASM

  SOLVING_PERCENTAGE = 75

  belongs_to :user
  has_many :test_questions, dependent: :destroy
  has_many :questions, through: :test_questions
  has_many :answers, through: :test_questions

  validates :correct_answers, :correct_answers_percentage,
            :questions_count, presence: true
  validates :questions_count, numericality: {
    only_integer: true, greater_than: 0
  }
  validate :question_limit

  aasm column: :state do
    state :started, initial: true
    state :finished

    event :finish do
      transitions from: :started, to: :finished
    end
  end

  def self.generate(user)
    test = user.tests.new
    test.generate_questions
  end

  def generate_questions
    limit_value = Settings.questions_per_block

    QuestionBlock.includes(:questions).all.each do |block|
      questions << block.questions.sample(limit_value)
      self.questions_count += limit_value
    end

    self
  end

  def resolve(update_params)
    update_params ? resolve_with_answers(update_params) : resolve_without_any_answer
  end

  private

  def resolve_without_any_answer
    now = Time.zone.now
    time_difference = TimeDifference.between(created_at, now)
                                    .in_minutes.to_i

    update(correct_answers: 0, solved_at: now,
           correct_answers_percentage: 0,
           is_solved: false,
           solving_time: time_difference)
  end

  def resolve_with_answers(update_params)
    Test.transaction do
      update_params.each do |test_question_id, answer_id|
        update_test_questions_answers(test_question_id, answer_id)
      end
      answers_count = count_correct_answers
      percentage = (answers_count.to_f / questions_count * 100.0).to_i
      solving_status = percentage >= SOLVING_PERCENTAGE
      now = Time.zone.now
      time_difference = TimeDifference.between(created_at, now)
                                      .in_minutes.to_i

      update(correct_answers: answers_count, solved_at: now,
             correct_answers_percentage: percentage,
             is_solved: solving_status,
             solving_time: time_difference)
    end
  end

  def question_limit
    errors.add(:questions, 'count cannot be zero') if questions.empty?
  end

  def update_test_questions_answers(test_question_id, answer_id)
    test_question = test_questions.find { |r| r.id == test_question_id.to_i }
    test_question.update(answer_id: answer_id)
  end

  def count_correct_answers
    correct_answers_counter = 0

    test_questions.includes(:answer).each do |test_question|
      correct_answers_counter += 1 if test_question.answer&.is_correct?
    end

    correct_answers_counter
  end
end

# == Schema Information
#
# Table name: tests
#
#  id                         :bigint           not null, primary key
#  correct_answers            :integer          default(0), not null
#  correct_answers_percentage :integer          default(0), not null
#  is_solved                  :boolean          default(FALSE), not null
#  questions_count            :integer          default(0), not null
#  solved_at                  :datetime
#  solving_time               :integer
#  state                      :enum             default("started"), not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  user_id                    :bigint           not null
#
# Indexes
#
#  index_tests_on_user_id  (user_id)
#
