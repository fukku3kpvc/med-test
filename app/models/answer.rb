class Answer < ApplicationRecord
  belongs_to :question
  has_many :test_questions, dependent: :destroy
  has_many :tests, through: :test_questions

  validates :context, presence: true
end

# == Schema Information
#
# Table name: answers
#
#  id          :bigint           not null, primary key
#  context     :text             default(""), not null
#  is_correct  :boolean          default(FALSE), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  question_id :bigint           not null
#
# Indexes
#
#  index_answers_on_question_id  (question_id)
#
