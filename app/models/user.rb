class User < ApplicationRecord
  devise :database_authenticatable, :rememberable, :validatable

  has_many :tests, dependent: :destroy

  validates :full_name, :login, :stored_password, presence: true
  validates_uniqueness_of :login

  PASSWORD_LENGTH = Settings.users.password_len.to_i


  class << self
    def new_self(full_name)
      login = generate_uniq_login
      pass = Devise.friendly_token.first(PASSWORD_LENGTH)
      enc_pass = Encryptor.enc(pass)

      new(full_name: full_name, login: login, password: pass,
          stored_password: enc_pass, password_confirmation: pass)
    end

    private

    def generate_uniq_login
      uniq_id = last.nil? ? '1' : last.id.next

      "#{FFaker::InternetSE.slug}_#{uniq_id}"
    end
  end

  def email_required?
    false
  end

  def will_save_change_to_email?
    false
  end
end

# == Schema Information
#
# Table name: users
#
#  id                  :bigint           not null, primary key
#  encrypted_password  :string           default(""), not null
#  full_name           :string           default(""), not null
#  login               :string           default(""), not null
#  remember_created_at :datetime
#  stored_password     :string           default(""), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_users_on_login  (login) UNIQUE
#
