class QuestionBlock < ApplicationRecord
  has_many :questions, dependent: :destroy

  validates :name, presence: true
end

# == Schema Information
#
# Table name: question_blocks
#
#  id         :bigint           not null, primary key
#  name       :string           default(""), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
