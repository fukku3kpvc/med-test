class Encryptor < ApplicationService
  def self.enc(text)
    new.enc(text)
  end

  def self.dec(text)
    new.dec(text)
  end

  def initialize
    @crypt = ActiveSupport::MessageEncryptor.new(build_key)
  end

  def enc(text)
    @crypt.encrypt_and_sign(text)
  end

  def dec(text)
    @crypt.decrypt_and_verify(text)
  end

  private

  def build_key
    base_key = Rails.application.credentials.secret_key_base
    base_salt = Rails.application.credentials.secret_salt_base

    ActiveSupport::KeyGenerator.new(base_key).generate_key(base_salt, 32)
  end
end
