module ApplicationHelper
  def is_solved?(test)
    test.is_solved? ? 'Пройден' : 'Не пройден'
  end
end
