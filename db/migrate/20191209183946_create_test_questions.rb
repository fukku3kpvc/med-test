class CreateTestQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :test_questions do |t|
      t.references :test, null: false
      t.references :question, null: false
      t.references :answer, null: true
      t.timestamps
    end
  end
end
