# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :full_name, null: false, default: ''
      t.string :login, null: false, default: ''
      t.string :encrypted_password, null: false, default: ''
      t.string :stored_password, null: false, default: ''
      t.datetime :remember_created_at

      t.timestamps null: false
    end
    add_index :users, :login, unique: true
  end
end
