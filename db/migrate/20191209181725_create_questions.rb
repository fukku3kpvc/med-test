class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.references :question_block, null: false
      t.text :context, null: false, default: ''
      t.timestamps
    end
  end
end
