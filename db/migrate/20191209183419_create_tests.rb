class CreateTests < ActiveRecord::Migration[5.2]
  def change
    execute <<-DDL
      CREATE TYPE test_status AS ENUM (
        'started',
        'finished'
      );
    DDL

    create_table :tests do |t|
      t.references :user, null: false
      t.column :state, :test_status, null: false, default: 'started'
      t.integer :correct_answers, null: false, default: 0
      t.integer :correct_answers_percentage, null: false, default: 0
      t.integer :questions_count, null: false, default: 0
      t.boolean :is_solved, null: false, default: false
      t.integer :solving_time
      t.datetime :solved_at
      t.timestamps
    end
  end
end
