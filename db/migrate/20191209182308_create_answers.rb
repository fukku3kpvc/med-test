class CreateAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :answers do |t|
      t.references :question, null: false
      t.text :context, null: false, default: ''
      t.boolean :is_correct, null: false, default: false
      t.timestamps
    end
  end
end
