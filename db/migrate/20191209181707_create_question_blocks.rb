class CreateQuestionBlocks < ActiveRecord::Migration[5.2]
  def change
    create_table :question_blocks do |t|
      t.string :name, null: false, default: ''
      t.timestamps
    end
  end
end
