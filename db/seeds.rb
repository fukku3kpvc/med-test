QuestionBlock.create(name: 'A')
QuestionBlock.create(name: 'B')
QuestionBlock.create(name: 'C')

dump_path = File.join(Rails.root, 'db', 'dump.json')
dump = File.read(dump_path)
questions = JSON.parse(dump)
questions.each do |question|
  question_id = question['id'].to_i
  question_block_id = question['question_block_id'].to_i
  question_context = question['context']
  puts "Creating Question with question_block_id: #{question_block_id}
        question_context: #{question_context}"
  Question.create(
    question_block_id: question_block_id,
    context: question_context
  )

  question['answers'].each do |answer|
    answer_context = answer['context']
    is_correct = answer['is_correct']
    puts "Creating Answer with answer_context: #{answer_context}
          is_correct: #{is_correct}"
    Answer.create(
      question_id: question_id,
      context: answer_context,
      is_correct: is_correct
    )
  end
end